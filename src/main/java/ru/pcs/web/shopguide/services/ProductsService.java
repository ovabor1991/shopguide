package ru.pcs.web.shopguide.services;
import ru.pcs.web.shopguide.forms.ProductCardForm;
import ru.pcs.web.shopguide.models.Product;
import java.util.List;

public interface ProductsService {
    void addProduct(ProductCardForm form);
    List<Product> getAllProducts();
    void deleteProduct(Integer productId);
    Product getProduct(Integer productId);
    void updateProduct(Integer productId, ProductCardForm form);
}

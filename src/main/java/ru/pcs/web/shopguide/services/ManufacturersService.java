package ru.pcs.web.shopguide.services;
import ru.pcs.web.shopguide.forms.ManufacturerCardForm;
import ru.pcs.web.shopguide.models.Manufacturer;
import java.util.List;

public interface ManufacturersService {
    void addManufacturer(ManufacturerCardForm form);
    List<Manufacturer> getAllManufacturers();
    Manufacturer getManufacturer(Integer manufacturerId);
    void deleteManufacturer(Integer manufacturerId);
    void updateManufacturer(Integer manufacturerId, ManufacturerCardForm form);

    List<Manufacturer> getManufacturersByProduct(Integer productId);
    List<Manufacturer> getManufacturersWithoutProduct();
    void addManufacturerToProduct(Integer productId, Integer manufacturerId);

}

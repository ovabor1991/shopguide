package ru.pcs.web.shopguide.services;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.web.shopguide.exeption.ProductNotFoundExeption;
import ru.pcs.web.shopguide.forms.ProductCardForm;
import ru.pcs.web.shopguide.models.Product;
import ru.pcs.web.shopguide.repositories.ProductsRepository;
import java.util.List;

@RequiredArgsConstructor
@Component
public class ProductsServiceImpl implements ProductsService {

    private final ProductsRepository productsRepository;

    @Override
    public void addProduct(ProductCardForm form) {
        Product product = Product.builder()
                .description(form.getDescription())
                .price(form.getPrice())
                .build();
        productsRepository.save(product);
    }

    @Override
    public List<Product> getAllProducts() {
        return productsRepository.findAll();
    }

    @Override
    public void deleteProduct(Integer productId) {
        productsRepository.deleteById(productId);
    }

    @Override
    public Product getProduct(Integer productId) {
        return productsRepository.findById(productId).orElseThrow(ProductNotFoundExeption::new);
    }

    @Override
    public void updateProduct(Integer productId, ProductCardForm form) {
        Product product = Product.builder()
                .id(productId)
                .description(form.getDescription())
                .price(form.getPrice())
                .build();
        productsRepository.save(product);
    }
}

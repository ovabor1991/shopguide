package ru.pcs.web.shopguide.services;
import lombok.RequiredArgsConstructor;
import org.springframework.stereotype.Component;
import ru.pcs.web.shopguide.exeption.ManufacturerNotFoundExeption;
import ru.pcs.web.shopguide.forms.ManufacturerCardForm;
import ru.pcs.web.shopguide.models.Manufacturer;
import ru.pcs.web.shopguide.models.Product;
import ru.pcs.web.shopguide.repositories.ManufacturersRepository;
import ru.pcs.web.shopguide.repositories.ProductsRepository;
import java.util.List;

@RequiredArgsConstructor
@Component
public class ManufacturerServiceImpl implements ManufacturersService {

    private final ManufacturersRepository manufacturersRepository;
    private final ProductsRepository productsRepository;

    @Override
    public List<Manufacturer> getManufacturersByProduct(Integer productId) {
        return manufacturersRepository.findAllByProduct_Id(productId);
    }

    @Override
    public List<Manufacturer> getManufacturersWithoutProduct() {
        return manufacturersRepository.findAllByProductIsNull();
    }

    @Override
    public void addManufacturerToProduct(Integer productId, Integer manufacturerId) {
        Product product = productsRepository.getById(productId);
        Manufacturer manufacturer = manufacturersRepository.getById(manufacturerId);
        manufacturer.setProduct(product);
        manufacturersRepository.save(manufacturer);
    }

    @Override
    public void addManufacturer(ManufacturerCardForm form) {
        Manufacturer manufacturer = Manufacturer.builder()
                .description(form.getDescription())
                .address(form.getAddress())
                .build();
        manufacturersRepository.save(manufacturer);
    }

    @Override
    public void deleteManufacturer(Integer manufacturerId) {
        manufacturersRepository.deleteById(manufacturerId);
    }

    @Override
    public List<Manufacturer> getAllManufacturers() {
        return manufacturersRepository.findAll();
    }

    @Override
    public Manufacturer getManufacturer(Integer manufacturerId) {
        return manufacturersRepository.findById(manufacturerId).orElseThrow(ManufacturerNotFoundExeption::new);
    }

    @Override
    public void updateManufacturer(Integer manufacturerId, ManufacturerCardForm form) {
        Manufacturer manufacturer = Manufacturer.builder()
                .id(manufacturerId)
                .description(form.getDescription())
                .address(form.getAddress())
                .build();
        manufacturersRepository.save(manufacturer);
    }
}

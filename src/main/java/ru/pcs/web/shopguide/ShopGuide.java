package ru.pcs.web.shopguide;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class ShopGuide {

    public static void main(String[] args) {
        SpringApplication.run(ShopGuide.class, args);
    }
}

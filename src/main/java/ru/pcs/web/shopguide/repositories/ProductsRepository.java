package ru.pcs.web.shopguide.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.shopguide.models.Product;

public interface ProductsRepository extends JpaRepository<Product, Integer> {

}

package ru.pcs.web.shopguide.repositories;
import org.springframework.data.jpa.repository.JpaRepository;
import ru.pcs.web.shopguide.models.Manufacturer;
import java.util.List;

public interface ManufacturersRepository extends JpaRepository<Manufacturer, Integer> {
    List<Manufacturer> findAllByProduct_Id(Integer id);
    List<Manufacturer> findAllByProductIsNull();
}

package ru.pcs.web.shopguide.forms;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotEmpty;

@Data
public class ProductCardForm {
    @NotEmpty
    @Length(max = 50)
    private String description;

    private double price;

}

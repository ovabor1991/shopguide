package ru.pcs.web.shopguide.forms;
import lombok.Data;
import org.hibernate.validator.constraints.Length;
import javax.validation.constraints.NotEmpty;

@Data
public class ManufacturerCardForm {
    private Integer id;

    @NotEmpty
    @Length(max=50)
    private String description;

    @NotEmpty
    @Length(max=50)
    private String address;
}

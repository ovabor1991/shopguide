package ru.pcs.web.shopguide.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pcs.web.shopguide.forms.ProductCardForm;
import ru.pcs.web.shopguide.models.Product;
import ru.pcs.web.shopguide.services.ProductsService;
import javax.validation.Valid;
import java.util.List;

@Controller
public class ProductsController {

    private final ProductsService productsService;


    @Autowired
    public ProductsController(ProductsService productsService) {
        this.productsService = productsService;
    }

    @GetMapping("/products")
    public String getProductsPage(Model model){
        List<Product> products = productsService.getAllProducts();
        model.addAttribute("products", products);
        return "products";
    }

    @GetMapping("/products/{product-id}")
    public String getProductPage(Model model, @PathVariable("product-id") Integer productId){
        Product product = productsService.getProduct(productId);
        model.addAttribute("product", product);
        return "product";
    }

    @PostMapping("/products")
    public String addProduct(@Valid ProductCardForm form, BindingResult result, RedirectAttributes forRedirectModel) {
        if (result.hasErrors()){
                forRedirectModel.addFlashAttribute("errors", "Введены некорректные данные!");
                return "redirect:/products";
        }
        productsService.addProduct(form);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/delete")
    public String deleteProduct(@PathVariable("product-id") Integer productId){
        productsService.deleteProduct(productId);
        return "redirect:/products";
    }

    @PostMapping("/products/{product-id}/update")
    public String updateProduct(@PathVariable("product-id") Integer productId, @Valid ProductCardForm form, BindingResult result, RedirectAttributes forRedirectModel) {
        if (result.hasErrors()){
            forRedirectModel.addFlashAttribute("errors", "Введены некорректные данные!");
            return "redirect:/products/{product-id}";
        }
        productsService.updateProduct(productId, form);
        return "redirect:/products";
    }
}

package ru.pcs.web.shopguide.controller;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.validation.BindingResult;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.servlet.mvc.support.RedirectAttributes;
import ru.pcs.web.shopguide.forms.ManufacturerCardForm;
import ru.pcs.web.shopguide.models.Manufacturer;
import ru.pcs.web.shopguide.services.ManufacturersService;

import javax.validation.Valid;
import java.util.List;


@Controller
public class ManufacturersController {

    private final ManufacturersService manufacturersService;

    @Autowired
    public ManufacturersController(ManufacturersService manufacturersService){
        this.manufacturersService = manufacturersService;
    }

    @GetMapping("/products/{product-id}/manufacturers")
    public String getManufacturersByProduct(Model model, @PathVariable("product-id") Integer productId){
        List<Manufacturer> manufacturers = manufacturersService.getManufacturersByProduct(productId);
        List<Manufacturer> unusedManufacturers = manufacturersService.getManufacturersWithoutProduct();
        model.addAttribute("productId", productId);
        model.addAttribute("manufacturers", manufacturers);
        model.addAttribute("unusedManufacturers", unusedManufacturers);
        return "manufacturers_of_product";
    }

    @PostMapping("/products/{product-id}/manufacturers")
    public String addManufacturerToProduct(@PathVariable("product-id") Integer productId, @RequestParam("manufacturerId") Integer manufacturerId){
        manufacturersService.addManufacturerToProduct(productId, manufacturerId);
        return "redirect:/products/" + productId + "/manufacturers";
    }

    @PostMapping("/manufacturers")
    public String addManufacturer(@Valid ManufacturerCardForm form, BindingResult result, RedirectAttributes forRedirectModel) {
        if (result.hasErrors()){
            forRedirectModel.addFlashAttribute("errors", "Введены некорректные данные!");
            return "redirect:/manufacturers";
        }
        manufacturersService.addManufacturer(form);
        return "redirect:/manufacturers";
    }

    @PostMapping("/manufacturers/{manufacturer-id}/delete")
    public String deleteManufacturer(@PathVariable("manufacturer-id") Integer manufacturerId){
        manufacturersService.deleteManufacturer(manufacturerId);
        return "redirect:/manufacturers";
    }

    @GetMapping("/manufacturers")
    public String getManufacturersPage(Model model){
        List<Manufacturer> manufacturers = manufacturersService.getAllManufacturers();
        model.addAttribute("manufacturers", manufacturers);
        return "manufacturers";
    }

    @GetMapping("/manufacturers/{manufacturer-id}")
    public String getManufacturerPage(Model model, @PathVariable("manufacturer-id") Integer manufacturerId){
        Manufacturer manufacturer = manufacturersService.getManufacturer(manufacturerId);
        model.addAttribute("manufacturer", manufacturer);
        return "manufacturer";
    }


    @PostMapping("/manufacturers/{manufacturer-id}/update")
    public String updateManufacturer(@PathVariable("manufacturer-id") Integer manufacturerId, @Valid ManufacturerCardForm form, BindingResult result, RedirectAttributes forRedirectModel) {
        if (result.hasErrors()){
            forRedirectModel.addFlashAttribute("errors", "Введены некорректные данные!");
            return "redirect:/manufacturers/{manufacturer-id}";
        }
        manufacturersService.updateManufacturer(manufacturerId, form);
        return "redirect:/manufacturers";
    }

}
